# AwSW - Community Resource and Assets Pack

Community Resource and Assets Pack (CRAP) is a community project for the game Angels with Scaly Wings focused on creating a mod, which any modder can use as a dependency of their mod, giving them access to extra sprites, backgrounds, sounds, etc. created or aquired by other members of the modding community.

## Credits:

+ Jakzie, editing base-game assets: `cr/kevin_smile.png`, `cr/kevin_think.png`, `cr/kevin_sad.png`
+ 4onen, editing base-game assets: `bg/crapannalab.png`, `bg/crapannalabpapersboxl.png`, `bg/crapannalabpapersboxr.png`, `cr/ipsum_happy_notail.png`, `cr/ipsum_normal_notail.png`, `cr/ipsum_sad_notail.png`, `cr/remy_look_ud.png`, `cr/remy_normal_ud.png`, `cr/remy_shy_ud.png`, `cr/remy_smile_ud.png`, `cr/remy_shy_ud+closed.png`, `cr/remy_shy_ud/leye.png`, `cr/maverick_annoyed.png`, `cr/maverick_despair.png`, `cr/maverick_irritated.png`, `cr/maverick_lost.png`, `cr/maverick_nice_lost.png`
+ AzrynFolf, editing base-game assets: `cr/bryce_pant.png`, `cr/bryce_pant_flip.png`, `cr/bryce_pantflirt.png`, `cr/bryce_pantflirt_flip.png`
+ Ryann, editing base-game assets: `cr/lorem_blush.png`, `cr/lorem_normal_blush.png`, `cr/lorem_scared.png`, `cr/lorem_sleep_blush.png`, `cr/lorem_sleep.png`
+ Volar Vie, editing base-game assets: `cr/remy_scared.png`, `cr/remy_scared_look.png`, `cr/remy_scared_look2.png`, `cr/remy_scared_look_forward.png`
+ Nacabo, editing base-game assets: `cr/damion_normal_cl.png`, `cr/damion_face_cl.png`
